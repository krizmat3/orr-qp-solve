use orr_qp_solve as qp;

use std::time;

// Beware that this calculation is largely dependent on the OS and its scheduler
// speed of execution may and will vary significantly, thus this is only a rough
// estimate of the performance of the solver.

// 250 - dimensional QP problem is the largest problem that can be solved within
// Linux stack size limit, for larger problems, one-time heap allocation is required
// for example using Box::new().
const D: usize = 250;
const P: usize = 50;

fn main() {

    // Creates random symmetric positive definite matrix
    let h = {
        let mut h = qp::SquareMatrix::<D>::new_random();
        h = (h + h.transpose()).scale(0.5);
        h + qp::SquareMatrix::<D>::identity().scale(D as f64)
    };
    
    // Creates random bound vectors
    let (x_upper, x_lower) = {
        let mut x_upper = qp::Vector::<D>::new_random();
        let mut x_lower = qp::Vector::<D>::new_random();
        x_upper.add_scalar_mut(1.0);
        x_lower.add_scalar_mut(-1.0);
        (x_upper, x_lower)
    };
    
    // Creates random initial guess
    let x_0 = {
        let mut x_0 = qp::Vector::<D>::new_random();
        x_0 = x_0.add_scalar(-0.5).scale(4.0);
        qp::saturate(&mut x_0, &x_upper, &x_lower);
        x_0
    };
    
    // Creates QP problem
    let mut problem = qp::Problem::new(h, 1.0, 1.95 / h.norm(), 1e-6).unwrap();
    
    let mut elapsed_time_list = [time::Duration::ZERO; P];

    for p in elapsed_time_list.iter_mut() {

        // Creates random vector c
        let c = qp::Vector::<D>::new_random();

        // Updates epsilon to be 1e-6 * ||c||
        problem.epsilon = 1e-6 * c.norm();

        let start_time = time::SystemTime::now();

        // Solves the QP problem
        let _x = problem.optimize(&c, &x_0, &x_upper, &x_lower);
        
        let elapsed_time = start_time.elapsed().unwrap();

        *p = elapsed_time;
    }
    
    elapsed_time_list.sort_unstable();

    println!("average = {:?}", elapsed_time_list.iter().sum::<time::Duration>()/elapsed_time_list.len() as u32);
    println!("median  = {:?}", elapsed_time_list[elapsed_time_list.len()/2]);
    println!("maximum = {:?}", elapsed_time_list.iter().max().unwrap());
    println!("minimum = {:?}", elapsed_time_list.iter().min().unwrap());

}
