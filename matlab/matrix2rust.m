function [] = matrix2rust(filename, array)
    fileID = fopen(filename, "w");

    if isvector(array)
        fprintf(fileID, 'na::vector![');
        for i = 1:length(array)
            fprintf(fileID, '%f', array(i));
            if i ~= length(array)
                fprintf(fileID, ', ');
            end
        end
    else
        fprintf(fileID, 'na::matrix![\n');
        for row = 1:size(array, 1)
            for col = 1:size(array, 2)
                fprintf(fileID, '%f', array(row, col));
                if col ~= size(array, 2)
                    fprintf(fileID, ', ');
                end
            end
            fprintf(fileID, ';\n');
        end
    end
    fprintf(fileID, ']');
    fclose(fileID);
end

