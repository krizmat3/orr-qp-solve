#![cfg_attr(not(test), no_std)]

use nalgebra as na;

#[cfg(feature = "f32")]
type F = f32;

#[cfg(not(feature = "f32"))]
type F = f64;

pub type SquareMatrix<const D: usize> = na::SMatrix<F, D, D>;
pub type Vector<const D: usize> = na::SVector<F, D>;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum ProblemError {
    InvalidConstant,
    NonHermitianMatrix,
    Unspecified,
}

type ProblemResult<T> = Result<T, ProblemError>;

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Problem<const D: usize> {
    h: SquareMatrix<D>,
    h_inv: SquareMatrix<D>,
    pub gamma: F,
    pub alpha: F,
    pub epsilon: F,
}

// Creating a new problem.
impl<const D: usize> Problem<D> {
    /// # New
    /// This function creates a new problem.
    ///
    /// ## Parameters
    /// - `h`: Matrix of the problem.
    /// - `gamma`: Gamma constant: deciding when to use proportional x_k.
    /// - `alpha`: Alpha constant: step size for proportioning step.
    /// - `epsilon`: Epsilon constant: deciding when to stop the optimization.
    ///
    /// ## Returns
    /// - Result of the creation.
    pub fn new(h: SquareMatrix<D>, gamma: F, alpha: F, epsilon: F) -> ProblemResult<Self> {

        #[cfg(not(feature = "diasable_checks"))] {
            if gamma <= 0.0 {
            return Err(ProblemError::InvalidConstant);
            }
            if alpha <= 0.0 || alpha >= 2.0 / h.norm() {
                return Err(ProblemError::InvalidConstant);
            }
            if epsilon <= 0.0 {
                return Err(ProblemError::InvalidConstant);
            }
        }
        
        let h_inv = h
            .cholesky()
            .ok_or(ProblemError::NonHermitianMatrix)?
            .inverse();

        return Ok(Problem {
            h,
            h_inv,
            gamma,
            alpha,
            epsilon,
        });
    }
}

// Land of gradients.
impl<const D: usize> Problem<D> {
    /// # Gradient
    /// This function calculates the gradient of the problem.
    ///
    /// ## Parameters
    /// - `c`: Vector of constants.
    /// - `x`: Vector of variables.
    ///
    /// ## Returns
    /// - Vector of gradients.
    pub fn gradient(&self, c: &Vector<D>, x: &Vector<D>) -> Vector<D> {
        return self.h * x + c;
    }

    /// # Gradient at
    /// This function calculates the gradient of the problem at a specific index.
    ///
    /// ## Parameters
    /// - `c`: Vector of constants.
    /// - `x`: Vector of variables.
    /// - `index`: Index of the gradient.
    pub fn gradient_at(&self, c: &Vector<D>, x: &Vector<D>, index: usize) -> F {
        return (self.h.row(index) * x).as_scalar() + c[index];
    }

    /// # Free gradient
    /// This function calculates the free gradient of the problem.
    /// Free gradient is gradient of all values of vector x that are not on the
    /// bounds.
    ///
    /// ## Parameters
    /// - `c`: Vector of constants.
    /// - `x`: Vector of variables.
    /// - `x_upper`: Vector of upper bounds.
    /// - `x_lower`: Vector of lower bounds.
    /// - `g`: Optional gradient vector, if already calculated.
    ///
    /// ## Returns
    /// - Vector of free gradients.
    fn free_gradient(
        &self,
        c: &Vector<D>,
        x: &Vector<D>,
        x_upper: &Vector<D>,
        x_lower: &Vector<D>,
        g: Option<&Vector<D>>,
    ) -> Vector<D> {
        let mut phi = Vector::<D>::zeros();

        for i in 0..D {
            if x[i] < x_upper[i] && x[i] > x_lower[i] {
                phi[i] = g
                    .map(|g_inner| g_inner[i])
                    .unwrap_or(self.gradient_at(c, x, i));
            }
        }

        return phi;
    }

    /// # Chopped gradient
    /// This function calculates the chopped gradient of the problem.
    /// Chopped gradient is gradient of all values of vector x that are on the
    /// bounds.
    ///
    /// ## Parameters
    /// - `c`: Vector of constants.
    /// - `x`: Vector of variables.
    /// - `x_upper`: Vector of upper bounds.
    /// - `x_lower`: Vector of lower bounds.
    /// - `g`: Optional gradient vector, if already calculated.
    ///
    /// ## Returns
    /// - Vector of chopped gradients.
    fn chopped_gradient(
        &self,
        c: &Vector<D>,
        x: &Vector<D>,
        x_upper: &Vector<D>,
        x_lower: &Vector<D>,
        g: Option<&Vector<D>>,
    ) -> Vector<D> {
        let mut beta = Vector::<D>::zeros();

        for i in 0..D {
            if x[i] >= x_upper[i] {
                beta[i] = g
                    .map(|g_inner| g_inner[i])
                    .unwrap_or(self.gradient_at(c, x, i))
                    .max(0.0);
            } else if x[i] <= x_lower[i] {
                beta[i] = g
                    .map(|g_inner| g_inner[i])
                    .unwrap_or(self.gradient_at(c, x, i))
                    .min(0.0);
            }
        }

        return beta;
    }

    /// # Projected gradient
    /// This function calculates the projected gradient of the problem.
    /// Projected gradient is gradient that has the values on the bounds cut,
    /// so they do not get out of the bounds.
    ///
    /// ## Parameters
    /// - `c`: Vector of constants.
    /// - `x`: Vector of variables.
    /// - `x_upper`: Vector of upper bounds.
    /// - `x_lower`: Vector of lower bounds.
    /// - `g`: Optional gradient vector, if already calculated.
    ///
    /// ## Returns
    /// - Vector of projected gradients.
    fn projected_gradient(
        &self,
        c: &Vector<D>,
        x: &Vector<D>,
        x_upper: &Vector<D>,
        x_lower: &Vector<D>,
        g: Option<&Vector<D>>,
    ) -> Vector<D> {
        let phi = self.free_gradient(c, x, x_upper, x_lower, g);
        let beta = self.chopped_gradient(c, x, x_upper, x_lower, g);

        return phi + beta;
    }
}

// Direction
impl<const D: usize> Problem<D> {
    /// # Direction
    /// This function calculates the direction using inverse of the marix H.
    ///
    /// ## Parameters
    /// - `c`: Vector of constants.
    /// - `x`: Vector of variables.
    /// - `x_upper`: Vector of upper bounds.
    /// - `x_lower`: Vector of lower bounds.
    /// - `g`: Optional gradient vector, if already calculated.
    ///
    /// ## Returns
    /// - Vector of direction.
    fn direction(
        &self,
        c: &Vector<D>,
        x: &Vector<D>,
        x_upper: &Vector<D>,
        x_lower: &Vector<D>,
        g: Option<&Vector<D>>,
    ) -> Vector<D> {
        let mut d = Vector::<D>::zeros();

        let local_g = self.gradient(c, x);
        let g = g.unwrap_or(&local_g);

        for i in 0..D {
            if x[i] < x_upper[i] && x[i] > x_lower[i] {
                d[i] = *(self.h_inv.row(i) * g).as_scalar();
            }
        }

        return d;
    }

    /// # H * direction
    /// This function calculates the H * direction in somewhat more efficient way.
    ///
    /// ## Parameters
    /// - `c`: Vector of constants.
    /// - `x`: Vector of variables.
    /// - `x_upper`: Vector of upper bounds.
    /// - `x_lower`: Vector of lower bounds.
    /// - `g`: Optional gradient vector, if already calculated.
    /// - `d`: Optional direction vector, if already calculated.
    ///
    /// ## Returns
    /// - Vector of H * direction.
    fn h_direction(
        &self,
        c: &Vector<D>,
        x: &Vector<D>,
        x_upper: &Vector<D>,
        x_lower: &Vector<D>,
        g: Option<&Vector<D>>,
        d: Option<&Vector<D>>,
    ) -> Vector<D> {
        let g = *g.unwrap_or(&self.gradient(c, x));
        let local_d = self.direction(c, x, x_upper, x_lower, Some(&g));
        let d = d.unwrap_or(&local_d);

        let mut h_direction = Vector::<D>::zeros();

        for i in 0..D {
            h_direction[i] = if x[i] < x_upper[i] && x[i] > x_lower[i] {
                *(self.h.row(i) * d).as_scalar()
            } else {
                g[i]
            };
        }

        return h_direction;
    }
}

// PLS
impl<const D: usize> Problem<D> {
    /// # Projected line search
    /// This function calculates the projected line search as described in
    /// _J. Nocedal and S. Wright, Numerical Optimization, Springer, New York, 1999._
    ///
    /// ## Parameters
    /// - `c`: Vector of constants.
    /// - `x`: Vector of variables.
    /// - `x_upper`: Vector of upper bounds.
    /// - `x_lower`: Vector of lower bounds.
    /// - `g`: Optional gradient vector, if already calculated.
    /// - `d`: Optional direction vector, if already calculated.
    ///
    /// ## Returns
    /// - Tuple containing result and next gradient.
    fn projected_line_search(
        &self,
        c: &Vector<D>,
        x: &Vector<D>,
        x_upper: &Vector<D>,
        x_lower: &Vector<D>,
        g: Option<&Vector<D>>,
        d: Option<&Vector<D>>,
    ) -> (Vector<D>, Vector<D>) {
        let mut g = *g.unwrap_or(&self.gradient(c, x));

        let d_local = self.direction(c, x, x_upper, x_lower, Some(&g));
        let d = d.unwrap_or(&d_local);

        let (t, t_sorted) = {
            let t = find_breakpoints(x, x_upper, x_lower, d);
            let mut t_sorted = t;
            t_sorted.sort_unstable_by(|a, b| unsafe { a.partial_cmp(b).unwrap_unchecked() });
            (t, t_sorted)
        };

        let mut x_prew = *x;
        let mut t_prew = 0.0;

        for t_i in t_sorted {
            if t_i <= t_prew {
                continue;
            }

            let max_t_change = t_i - t_prew;

            let mut d_prew = Vector::<D>::zeros();

            for i in 0..D {
                if t_prew < t[i] {
                    d_prew[i] = -d[i];
                }
            }

            let h_d = self.h_direction(c, x, x_upper, x_lower, Some(&g), Some(d));

            let f1 = c.dot(&d_prew) + x_prew.dot(&h_d);
            let f2 = d_prew.dot(&h_d);

            let best_t_change = if f1 == 0. { 0. } else { -f1 / f2 };
            if 0.0 <= best_t_change && best_t_change < max_t_change && f1 > 0.0 {
                x_prew += best_t_change * d_prew;
                g -= best_t_change * h_d;
                break;
            }

            t_prew = t_i;
            x_prew += max_t_change * d_prew;
            g -= max_t_change * h_d;
        }

        return (x_prew, g);
    }
}

impl<const D: usize> Problem<D> {
    #[inline]
    /// # Optimize
    /// This function calculates the optimal solution of the problem.
    ///
    /// ## Parameters
    /// - `c`: Vector of constants.
    /// - `x_0`: Vector of initial variables.
    /// - `x_upper`: Vector of upper bounds.
    /// - `x_lower`: Vector of lower bounds.
    ///
    /// ## Returns
    /// - Vector of optimal variables.
    pub fn optimize(
        &self,
        c: &Vector<D>,
        x_0: &Vector<D>,
        x_upper: &Vector<D>,
        x_lower: &Vector<D>,
    ) -> Vector<D> {
        let mut x_k = *x_0;
        let mut g_k = self.gradient(c, &x_k);

        #[cfg(test)]
        let mut iterations: usize = 0;

        while self
            .projected_gradient(c, &x_k, x_upper, x_lower, Some(&g_k))
            .norm()
            >= self.epsilon
        {
            #[cfg(test)]
            {
                iterations += 1;
            }

            if self
                .chopped_gradient(c, &x_k, x_upper, x_lower, Some(&g_k))
                .norm()
                <= self.gamma
                    * self
                        .free_gradient(c, &x_k, x_upper, x_lower, Some(&g_k))
                        .norm()
            {
                let d_k = self.direction(c, &x_k, x_upper, x_lower, Some(&g_k));
                let h_d = self.h_direction(c, &x_k, x_upper, x_lower, Some(&g_k), Some(&d_k));

                let alpha_f = max_alpha(&x_k, x_upper, x_lower, &d_k);
                if alpha_f < 1.0 {
                    (x_k, g_k) = self.projected_line_search(
                        c,
                        &x_k,
                        x_upper,
                        x_lower,
                        Some(&g_k),
                        Some(&d_k),
                    );
                } else {
                    x_k -= d_k;
                    g_k -= h_d;
                }
            } else {
                x_k -= self.alpha * self.chopped_gradient(c, &x_k, x_upper, x_lower, Some(&g_k));
                saturate(&mut x_k, x_upper, x_lower);
                g_k = self.gradient(c, &x_k);
            }
        }

        #[cfg(test)]
        println!("Iterations: {}", iterations);

        return x_k;
    }
}

fn find_breakpoints<const D: usize>(
    x: &Vector<D>,
    x_upper: &Vector<D>,
    x_lower: &Vector<D>,
    d: &Vector<D>,
) -> [F; D] {
    let mut t = [F::INFINITY; D];

    for i in 0..D {
        if d[i] < 0. && x_upper[i] < F::INFINITY {
            t[i] = (x[i] - x_upper[i]) / d[i];
        } else if d[i] > 0. && x_lower[i] > -F::INFINITY {
            t[i] = (x[i] - x_lower[i]) / d[i];
        }
    }
    return t;
}

fn max_alpha<const D: usize>(
    x: &Vector<D>,
    x_upper: &Vector<D>,
    x_lower: &Vector<D>,
    d: &Vector<D>,
) -> F {
    return *find_breakpoints(x, x_upper, x_lower, d)
        .iter()
        .min_by(|&a, &b| unsafe { a.partial_cmp(b).unwrap_unchecked() })
        .unwrap_or(&F::INFINITY);
}

pub fn saturate<const D: usize>(x: &mut Vector<D>, x_upper: &Vector<D>, x_lower: &Vector<D>) {
    for i in 0..D {
        x[i] = x[i].max(x_lower[i]).min(x_upper[i]);
    }
}

#[test]
fn basic_test() {
    let h = na::matrix![
        1., 1.;
        0., 1.
    ];

    let c = na::vector![1., 0.];

    let x_upper = na::vector![5., 5.];
    let x_lower = na::vector![-5., -5.];

    let x_0 = na::vector![1., 0.];

    let problem = Problem::new(h, 1.0, 1.95 / h.norm(), 1e-6 * c.norm()).unwrap();

    let x = problem.optimize(&c, &x_0, &x_upper, &x_lower);

    assert_eq!(x, na::vector![-1., 0.])
}
