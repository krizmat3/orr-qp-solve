# QP-Solver for ORR Semestral Project
Rust `no_std` QP-Solver library based on
[Proportioning with second-order information for MPC](https://doi.org/10.1080/10556788.2016.1213840).

This solver is implemented as `no_std`, therefore it lacks many features that are present in software
requiring heap allocation. The solver is designed to be used in embedded systems where heap allocation
is not available and for systems where the performance is critical and heap allocation is not desired.

It solves the following problem:
$$
\begin{aligned}
\min_{z} \quad & \frac{1}{2} x^T H z + c^T x \\
\text{s.t.} \quad & x_{\text{lower}} \leq x \leq x_{\text{upper}}
\end{aligned}
$$
where $H$ is a symmetric positive definite matrix, $c$ is a constant vector, $x_{\text{lower}}$ and $x_{\text{upper}}$
are vectors with lower and upper bounds for $x$.

This library is `no_std` compatible and can be used in embedded systems without allocator.

## Usage
Add the following to your `Cargo.toml`:
```toml
[dependencies]
orr-qp-solve = { git = "https://gitlab.fel.cvut.cz/krizmat3/orr-qp-solve" }
```

If you want to use `f32` (`float`) type instead of `f64` (`double`), you can enable the `f32` feature:
```toml
[dependencies]
orr-qp-solve = { git = "https://gitlab.fel.cvut.cz/krizmat3/orr-qp-solve", features = ["f32"] }
```

There is also `disable_checks` feature that disables some checks in the code. This can be useful for
embedded systems where the code is tested and the checks are not needed:
```toml
[dependencies]
orr-qp-solve = { git = "https://gitlab.fel.cvut.cz/krizmat3/orr-qp-solve" features = ["disable_checks"] }
```
Disables checks for `gamma`, `alpha`, and `epsilon` parameters.

## Conditions
There are conditions that must be met for the problem to be solvable:
- The matrix $H$ must be symmetric positive definite (SPD).
- Initial condition $z_0$ must be feasible.
- $\Gamma > 0$
- $\alpha \in (0, \frac{2}{||H||})$ 
- $\varepsilon > 0$

## Example
This example shows how to solve the following problem:
$$
\begin{aligned}
\min_{x} \quad & \frac{1}{2} x^T H x + c^T x \\
\text{s.t.} \quad & x_{\text{lower}} \leq x \leq x_{\text{upper}}
\end{aligned}
$$
where
$$
H = \begin{bmatrix}
1 & 1 \\
0 & 1
\end{bmatrix}, \quad
c = \begin{bmatrix}
1 \\
0
\end{bmatrix}, \quad
x_{\text{upper}} = \begin{bmatrix}
5 \\
5
\end{bmatrix}, \quad
x_{\text{lower}} = \begin{bmatrix}
-5 \\
-5
\end{bmatrix}, \quad
x_0 = \begin{bmatrix}
1 \\
0
\end{bmatrix}
$$

The parameters are set to:
$$
\Gamma = 1,\\
\alpha = \frac{1.95}{\|H\|},\\
\varepsilon = 10^{-6} \cdot \|c\|\\
$$

```rust
use orr_qp_solve as qp;

fn main() {
    let h = na::matrix![
        1., 1.;
        0., 1.
    ];

    let c = na::vector![1., 0.];

    let x_upper = na::vector![5., 5.];
    let x_lower = na::vector![-5., -5.];

    let x_0 = na::vector![1., 0.];

    let gamma = 1.0;
    let alpha = 1.0 / h.norm();
    let epsilon = 1e-6 * c.norm();

    let problem = na::Problem::new(h, gamma, alpha, epsilon).unwrap();

    let x = problem.optimize(&c, &x_0, &x_upper, &x_lower);

    assert_eq!(x, na::vector![-1., 0.])
}
```

## Description of the algorithm
### Problem initialization
The problem is initialized by creating a `Problem` struct. The struct contains the following fields:
- `h` - Symmetric positive definite matrix $H$.
- `h_inv` - Inverse of the matrix $H$, calculated using Cholesky decomposition at the time of initialization.
- parameters `gamma`, `alpha`, and `epsilon`.

Checks for the conditions mentioned above are performed during the initialization.

### Optimization
Algorithm is described in the aforementioned paper. It utilizes selective matrix multiplication to lower the
computational complexity of the algorithm.

## How to efficiently use the library
### Empty matrices
As matricies have to be static, it is best to create them as static variables and filling them in later.
```rust
let mut h = qp::SymmetricMatrix::<2>::zeros();
```
This creates a zero matrix of size 2x2. The matrix can be filled in later.

### Matrixies from other software
Other method is freating the matrix in other software and including it in the code.

If we create the matrix in MATLAB, we can store it in `matrix.txt` file as:
```
na::matrix![
1.0, 1.0;
0.0, 1.0;
]
```
Then we can import the matrix in Rust as:
```rust
let h = include!("matrix.txt");
```
which will determine the size and fill the matrix with the values from the file at compile time.

This way we can create the matrix in environment better suited for matrix manipulation.

In order to use this method, we need `nalgebra` crate included as a dependency.
```toml
[dependencies]
nalgebra = { version = "*", default-features = false, features = ["macros", "libm"] }
```
and use it as
```rust
use nalgebra as na;
```

For file generation there is also Matlab function `matrix_to_rust` included in the repository.

## Limitations
Currently there is limitation on the size of the matrix. As it is stored on the stack, the size of the matrix
is limited to $\approx 250 \times 250$ for `f64`. To further increase the size of the matrix, it is necessary
to use one-time heap allocation for the matrix and problem. This can be done by encasing the matrix in
```rust
let h = Box::new(na::matrix![...]);
```
and then doing the same for the problem.

This will allocate the space for the matrix on the heap and allow for larger problems.

## Performance comparison
WIP
